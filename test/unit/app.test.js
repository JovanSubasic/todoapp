const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const { assert } = require('chai');
const { TodoApp, Store } = require('../../src/app');

const dom = new JSDOM('<!doctype html><html><body></body></html>', {
    url: 'http://localhost:63342/todoapp/index.html',
    storageQuota: 10000000,
});

global.localStorage = dom.window.localStorage;


describe('TodoApp', function () {
    let store;
    let todoApp;

    beforeEach(function () {
        store = new Store();
        todoApp = new TodoApp(store);
    });

    afterEach(function () {
        localStorage.clear();
    });

    it('ajoute la tache à la liste', function () {
        todoApp.addTask('tache 1');
        assert.deepEqual(todoApp.getTasks(), ['tache 1']);
    });

    it('supprime la tache de la liste', function () {
        todoApp.addTask('tache 1');
        todoApp.removeTask(0);
        assert.deepEqual(todoApp.getTasks(), []);
    });

    it('ne supprime pas la tache si l\'index n\'est pas bon', function () {
        todoApp.addTask('tache 1');
        todoApp.removeTask(1);
        assert.deepEqual(todoApp.getTasks(), ['tache 1']);
    });
});

describe('Store', function () {
    let store;

    beforeEach(function () {
        store = new Store();
    });

    afterEach(function () {
        localStorage.clear();
    });

    it('initialise un tableau vide s\'il n\'y a pas de donnée', function () {
        assert.deepEqual(store.getData(), []);
    });

    it('sauvegarde les données', function () {
        store.data = ['tache 1'];
        store.saveData();
        assert.deepEqual(JSON.parse(localStorage.getItem('tasks')), ['tache 1']);
    });

});
