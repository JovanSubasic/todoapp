// src/app.js

class Store {
    constructor() {
        // Initialisez le store ou chargez les données existantes depuis un emplacement externe (ex : localStorage).
        this.data = JSON.parse(localStorage.getItem('tasks')) || [];
    }

    saveData() {
        // Sauvegardez les données dans le store (ou dans un emplacement externe).
        localStorage.setItem('tasks', JSON.stringify(this.data));
    }

    getData() {
        // Obtenez les données du store.
        return this.data;
    }

    // Ajoutez d'autres méthodes pour gérer les données au besoin.
}
class TodoApp {
    constructor(store) {
        this.store = store;
        this.tasks = this.store.getData();
    }

    addTask(task) {
        this.tasks.push(task);
        this.store.saveData();
    }

    removeTask(index) {
        this.tasks.splice(index, 1);
        this.store.saveData();
    }

    getTasks() {
        return this.tasks;
    }
}
module.exports = {
    TodoApp,
    Store,
};
